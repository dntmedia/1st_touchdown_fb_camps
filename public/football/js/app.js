
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});

$(document).ready(function() {
	$("#lightSlider").lightSlider({
		item:1,
        // loop:false,
        slideMove:2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:400,
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        pauseOnHover: true,
        controls: true,
        adaptiveHeight: false,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:650,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ]
	}); 
	$("#lightSlider2").lightSlider({
		item:1,
        // loop:false,
        slideMove:2,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:900,
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        pauseOnHover: true,
        controls: true,
        adaptiveHeight: false,
        responsive : [
            {
                breakpoint:800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:650,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ]
	}); 
});