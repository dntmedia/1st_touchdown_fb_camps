<?php

if (Config::get('basic.over_ride') == 0) {

    Route::group(['middleware' => 'web', 'as' => 'frontend.', 'namespace' => 'Modules\Basic\Http\Controllers'], function () {

        Route::get('/', 'FrontendController@index')->name('index');
        Route::get('macros', 'FrontendController@macros')->name('macros');

    });

}
