<?php

namespace Modules\Basic\Http\Controllers;

use App\Http\Controllers\Controller;
use View;

/**
 * Class FrontendController.
 */
class FrontendController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {

        // dd("here");

        // return view('frontend.index');
        return View::make('basic::frontend.index');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros()
    {
        // return view('frontend.macros');
        return View::make('basic::frontend.macros');
    }
}
