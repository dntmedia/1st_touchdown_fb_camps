<?php

namespace Modules\Basic\Http\Controllers;

use App\Http\Controllers\Controller;
use View;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // return view('frontend.user.dashboard');
        return View::make('basic::frontend.dashboard');
    }
}
