<?php

namespace Modules\Kiroku\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Kiroku\Repositories\EloquentHistoryRepository;
use Modules\Kiroku\Repositories\Facades\History as HistoryFacade;
use Modules\Kiroku\Repositories\HistoryContract;

/**
 * Class HistoryServiceProvider.
 */
class HistoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(HistoryContract::class, EloquentHistoryRepository::class);
        $this->app->bind('history', HistoryContract::class);
        $this->registerFacade();
    }

    public function registerFacade()
    {
        $this->app->booting(function () {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('History', HistoryFacade::class);
        });
    }
}
