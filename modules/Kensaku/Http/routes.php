<?php

Route::group(['middleware' => 'web', 'prefix' => 'kensaku', 'namespace' => 'Modules\Kensaku\Http\Controllers'], function () {
    Route::get('/', 'KensakuController@index');
});

// Route::group(['namespace' => 'Modules\Kensaku\Http\Controllers', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {

//      * These routes need view-backend permission
//      * (good if you want to allow more than one group in the backend,
//      * then limit the backend features by different roles or permissions)
//      *
//      * Note: Administrator has all permissions so you do not have to specify the administrator role everywhere.

//     Route::group([
//         'prefix' => 'search',
//         'as' => 'search.',
//     ], function () {

//         /*
//          * Search Specific Functionality
//          */
//         Route::get('/', 'SearchController@index')->name('index');
//     });

// });
