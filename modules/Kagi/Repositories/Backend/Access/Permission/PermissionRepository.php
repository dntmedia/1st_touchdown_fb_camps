<?php

namespace Modules\Kagi\Repositories\Backend\Access\Permission;

use App\Repositories\BaseRepository;
use Modules\Kagi\Models\Access\Permission\Permission;

/**
 * Class PermissionRepository.
 */
class PermissionRepository extends BaseRepository
{
    /**
     * Associated Repository Model.
     */
    const MODEL = Permission::class;
}
