<?php

Route::group(['middleware' => 'web', 'prefix' => 'kagi', 'namespace' => 'Modules\Kagi\Http\Controllers'], function()
{
    Route::get('/', 'KagiController@index');
});
