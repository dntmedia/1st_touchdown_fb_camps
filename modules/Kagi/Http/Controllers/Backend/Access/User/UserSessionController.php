<?php

namespace Modules\Kagi\Http\Controllers\Backend\Access\User;

use App\Http\Controllers\Controller;
use Modules\Kagi\Http\Requests\Backend\Access\User\ManageUserRequest;
use Modules\Kagi\Models\Access\User\User;
use Modules\Kagi\Repositories\Backend\Access\User\UserSessionRepository;

/**
 * Class UserSessionController.
 */
class UserSessionController extends Controller
{
    /**
     * @param User                  $user
     * @param ManageUserRequest     $request
     * @param UserSessionRepository $userSessionRepository
     *
     * @return mixed
     */
    public function clearSession(User $user, ManageUserRequest $request, UserSessionRepository $userSessionRepository)
    {
        $userSessionRepository->clearSession($user);

        return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.session_cleared'));
    }
}
