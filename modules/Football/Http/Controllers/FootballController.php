<?php

namespace Modules\Football\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class FootballController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('football::index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function schedule()
    {
        return view('football::schedule');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function success()
    {
        return view('football::football.success');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function info()
    {
        return view('football::info');
    }

}
