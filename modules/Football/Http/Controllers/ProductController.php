<?php

namespace Modules\Football\Http\Controllers;

use App\Http\Controllers\Controller;
use View;

class ProductController extends Controller
{
    /**
     * Show the index page.
     *
     * @var App\Product $products
     * @return Illuminate\View\View
     */
    public function index()
    {

        $products = \Modules\Football\Models\Product::all();

        // return view('index', compact('products'));
        return View::make('football::football.index',
            compact('products')
        );

    }
}
