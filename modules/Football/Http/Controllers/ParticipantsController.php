<?php

namespace Modules\Football\Http\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;
use Modules\Football\Models\Participant;
use Modules\Kagi\Models\Access\User\User;
use Session;
use View;

class ParticipantsController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $participants = Participant::all();

        // return view('participants.index')->withParticipants($participants);
        return View::make('football::participants.index',
            compact('participants')
        );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('participants.create');
        return View::make('football::participants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // dd($request);

        // $this->validate($request, [
        //     'first_name' => 'required',
        //     'last_name' => 'required',
        //     'home_phone' => 'required',
        //     'cell_phone' => 'required',
        //     'email' => 'required',
        //     'parent_first_name' => 'required',
        //     'parent_last_name' => 'required',
        //     'parent_phone' => 'required',
        // ]);

        // $input = $request->all();

        // Participant::create($input);

        // $values = [
        //     'name' => substr(strtolower($request['parent_first_name']), 0, 1) . ucfirst($request['parent_last_name']),
        //     'email' => $request['guardian_email'],
        //     'password' => bcrypt($request['guardian_email']),
        //     'admin' => 0,
        //     'confirmation_code' => md5(uniqid(mt_rand(), true)),
        //     'confirmed' => 1,
        // ];
        // //dd($values);

        // $user = User::create($values);
        // $user_id = $user->id;
        $user_id = 6;

        $products = \Modules\Football\Models\Product::all();

        $user = User::find($user_id);
        // dd($user);

        $logged_in = Auth::login($user);
        // dd($logged_in);

        // dd(Auth::check());

        // dd(Auth::loginUsingId($user_id));

        // Auth::attempt(['email' => $user->email, 'password' => $user->password]);

        // dd(Auth::user());

        return View::make('football::football.pay',
            compact(
                'products',
                'user_id'
            )
        );

        // Session::flash('flash_message', 'Participant has been added successfully');
        // return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $participant = Participant::findOrFail($id);

        // return view('participants.show')->withParticipant($participant);
        return View::make('football::participants.show',
            compact('participant')
        );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $participant = Participant::findOrFail($id);

        // return view('participants.edit')->withParticipant($participant);
        return View::make('football::participants.edit',
            compact('participant')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $participant = Participant::findOrFail($id);

        $this->validate($request, [

        ]);

        $input = $request->all();

        $participant->fill($input)->save();

        Session::flash('flash_message', 'Participant successfully updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $participant = Participant::findOrFail($id);

        $participant->delete();

        Session::flash('flash_message', 'Participant successfully deleted!');

        return redirect()->route('participants.index');
    }
}
