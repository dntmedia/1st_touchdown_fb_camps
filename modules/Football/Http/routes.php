<?php

Route::group(['middleware' => 'web', 'prefix' => 'football', 'namespace' => 'Modules\Football\Http\Controllers'], function () {
    Route::get('info', 'FootballController@info');
});

if (Config::get('basic.over_ride') == 1) {

    Route::get('/', 'Modules\Football\Http\Controllers\FootballController@index')->name('frontend.index');
    Route::get('macros', 'Modules\Football\Http\Controllers\FootballController@index')->name('frontend.macros');

    Route::group(['middleware' => 'web', 'as' => 'participants.', 'namespace' => 'Modules\Football\Http\Controllers'], function () {

        Route::get('schedule', 'FootballController@index')->name('schedule');

        Route::get('signup', 'ParticipantsController@create')->name('signup');
        Route::post('signup', 'ParticipantsController@store')->name('signup');

        Route::post('pay/{product}', 'OrderController@postPayWithStripe')->name('pay');

        Route::get('success', 'FootballController@success')->name('success');

    });

}

// Route::group(['middleware' => 'web', 'as' => 'admin.football.participants.', 'prefix' => 'admin/football', 'namespace' => 'Modules\Football\Http\Controllers'], function () {

//     Route::get('/', 'ParticipantsController@index')->name('index');
//     Route::get('{id}', 'ParticipantsController@show')->name('show');
//     Route::get('{id}', 'ParticipantsController@edit')->name('edit');
//     // Route::post('signup', 'ParticipantsController@store')->name('signup');

// });

Route::group(['middleware' => 'auth', 'prefix' => 'football', 'namespace' => 'Modules\Football\Http\Controllers'], function () {

    // Product Routes
    Route::get('/', [
        'uses' => 'ProductController@index',
        'as' => 'index',
        'middleware' => 'auth',
    ]);

    // Order Routes

    Route::post('/store', [
        'uses' => 'OrderController@postPayWithStripe',
        'as' => 'store',
        'middleware' => 'auth',
    ]);

});

Route::group(['middleware' => 'web', 'prefix' => 'admin/football', 'namespace' => 'Modules\Football\Http\Controllers'], function () {

    // Route::resource('participants', 'ParticipantsController');

    Route::get('/orders', [
        'uses' => 'OrderController@getAllOrders',
        'as' => 'admin.football.orders',
        'middleware' => 'admin',
    ]);

    Route::get('/participants', [
        'uses' => 'ParticipantsController@index',
        'as' => 'admin.football.participants',
        'middleware' => 'admin',
    ]);

    Route::get('/participants/{id}', [
        'uses' => 'ParticipantsController@show',
        'as' => 'admin.football.participants.show',
        'middleware' => 'admin',
    ]);

    Route::get('/participants/{id}/edit', [
        'uses' => 'ParticipantsController@edit',
        'as' => 'admin.football.participants.edit',
        'middleware' => 'admin',
    ]);

});
