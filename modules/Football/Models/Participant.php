<?php

namespace Modules\Football\Models;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{

    protected $table = 'football_participants';

    /**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'address',
        'city',
        'state',
        'zipcode',
        'home_phone',
        'cell_phone',
        'email',
        'guardian_email',
        'dob',
        'school',
        'primary_position',
        'secondary_position',
        'tshirt_size',
        'med_conditions',
        'parent_first_name',
        'parent_last_name',
        'parent_phone',
        'waiver_signature',
        'waiver_date',
        'emergency_first_name',
        'emergency_last_name',
        'emergency_phone',
        'emergency_alt_phone',
    ];
}
