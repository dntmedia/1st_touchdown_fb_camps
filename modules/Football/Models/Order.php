<?php

namespace Modules\Football\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $table = 'football_orders';

    protected $fillable = [
        'email',
        'product',
    ];

    public function user()
    {
        return $this->hasOne('Modules\Kagi\Models\Access\User\User');
    }

    public function product()
    {
        return $this->hasOne('Modules\Football\Models\Product');
    }
}
