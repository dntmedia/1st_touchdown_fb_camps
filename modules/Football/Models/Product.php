<?php

namespace Modules\Football\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = 'football_products';

}
