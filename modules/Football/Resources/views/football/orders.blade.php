@extends('backend.layouts.app')

@section('page-header')
    <h1>
        <small>{{ trans_choice('menus.backend.football.order',2) }}</small>
    </h1>
@endsection

@section('content')
<div class="row">
<div class="col-sm-12">

<table id="table" class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Email</th>
			<th>Product</th>
			<th>Date</th>
		</tr>
	</thead>
	<tbody>
	@foreach($orders as $order)
		<tr>
			<td>
				{{ $order->email }}
			</td>
			<td>
				{{ $order->product }}
			</td>
			<td>
				{{ $order->created_at }}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>


</div>
</div>
@stop
