@extends('frontend.layouts.football')

@section('content')


<section id="showcase">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="showcase-left">
              <img src="{{ url('football/images/image1.jpg') }}" class="img-responsive">
            </div>
          </div>
          <div class="col-md-6">
            <div class="showcase-right">
              <h1>Welcome to 1st Touchdown Football Camps</h1>
              <p class="lead">Youth Camp in Central Arkansas, dedicated to bringing the best sports experience to your young athlete in a positive and encouraging way!</p>
            </div>
            <br>
            <a href="{{ route('participants.schedule') }}" class="btn btn-default btn-lg showcase-btn">Schedule</a>
            <a href="{{ route('participants.signup') }}" class="btn btn-default btn-lg showcase-btn">Signup</a>
          </div>
        </div>
      </div>
    </section>

    <section id="testimonial">
      <div class="container">
        <h2>Hear what some of Coach Maupin's former players had to say...</h2>
        <div class="row">
          <div class="col-md-4">
            <div class="thumbnail">
              <h3>Myles Fells - RB</h3>
              <video height="350" controls src="{{ url('football/videos/video1.mov') }}"></video>
              <div class="caption">
                <img src="{{ url('football/images/navy_logo.png') }}" alt="Navy logo">
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="thumbnail">
              <h3>David Porter - DE/LB</h3>
              <video height="350" controls src="{{ url('football/videos/video2.mov') }}"></video>
              <div class="caption">
                <img src="{{ url('football/images/razorbacks_logo.png') }}" alt="Razorbacks logo">
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="thumbnail">
              <h3>Koilan Jackson - WR</h3>
              <video height="350" controls src="{{ url('football/videos/video3.mov') }}"></video>
              <div class="caption">
                <img src="{{ url('football/images/razorbacks_logo.png') }}" alt="Razorbacks logo">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="info1">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <div class="info-left">
              <img src="{{ url('football/images/image2.jpg') }}" class="img-responsive">
            </div>
          </div>
          <div class="col-md-6">
            <div class="info-right">
              <h2>I'm Coach Maupin</h2>
              <p>I've been married to my wife, Sallie for 5 years. We have two beautiful children. Junior (21mos) Taelor (4mos). I was an All State football Player at Robinson High school and went on to play Linebacker at Harding University. I teach Elementary PE at Robinson and Chenal Elementary. I also coach football and basketball at Robinson High School. With that, I handle all strength/conditioning, recruiting and have traveled over 20,000 miles across the country taking players to potential Universities to help them find a place they can further their education and be successful. I love kids and strive to give everything I have back to them in this community. I have trained and mentored student athletes like University of Arkansas RB/WR TJ Hammonds, WR Koilan Jackson, DE/LB David Porter, and Naval Academy RB Myles Fells.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section id="schedule">
      <div class="container schedule-bg">
        <h2>Camp Schedule</h2>
        <div class="row">
          <div class="col-md-6">
            <p><span>7:15am - 8:00am</span> - Drop off</p>
            <p><span>8:00am - 8:10am</span> - Camp call up</p>
            <p><span>8:10am - 8:15am</span> - Split into age specific groups K-2, 3-5, 6-8</p>
            <p><span>8:15am - 9:00am</span> - Start Athletic Development rotations (9 stations including 3 water/rest stations)</p>
            <p><span>9:00am - 9:15am</span> - Camp character development</p>
            <p><span>9:15am - 10:00am</span> - Start Skills and Drills Rotations (9 stations including 3 water/rest stations)</p>
            <p><span>10:00am - 10:15am</span> - Guest Speaker</p>
            <p><span>10:15am - 11:00am</span> - Competition Rotations</p>
            <p><span>11:00am - 11:15am</span> - Final Competition</p>
            <p><span>11:15am - 11:30am</span> - Camp Call up</p>
            <p><span>11:30am - 12:00pm</span> - Pick-up</p>
          </div>
          <div class="col-md-6">
            <div class="col-md-6">
               <div class="panel panel-default">
                  <div class="panel-heading panel-bg-1">
                    <div>
                      <h4 class="text-center">One Day <span>$50</span></h4>
                    </div>
                  </div>
                  <div class="panel-body">
                    Includes
                    <ul class="list-inline text-center">
                      <li>Wristband</li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="col-md-6">
               <div class="panel panel-default">
                  <div class="panel-heading panel-bg-2">
                    <div>
                      <h4 class="text-center">One Week <span>$175</span></h4>
                    </div>
                  </div>
                  <div class="panel-body">
                    Includes
                    <ul class="list-inline text-center">
                      <li>Wristband</li>
                      <li>T-shirt</li>
                    </ul>
                  </div>
                </div>
            </div>
            <div class="col-md-12">
               <div class="panel">
                  <div class="panel-heading panel-bg-3">
                    <h3 class="panel-title text-center">What to bring to camp?</h3>
                  </div>
                  <div class="panel-body">
                    <ul class="text-center">
                      <li><span class="glyphicon glyphicon-check"></span> Athletic Gear</li>
                      <li><span class="glyphicon glyphicon-check"></span> Cleats</li>
                      <li><span class="glyphicon glyphicon-check"></span> Sunscreen</li>
                      <li><span class="glyphicon glyphicon-check"></span> QBs - can bring their own football</li>
                    </ul>
                  </div>
                </div>
            </div>
          </div>
          <div class="col-md-12">
            <p class="text-center">* Pre-registration is guranteed swag gear</p>
            <p class="text-center">* Limited to 100 campers</p>
          </div>
        </div>
      </div>
    </section>
    <section id="info2">
      <div class="container text-center">
      	<div class="row">
      		<div class="col-md-4">
      			<img src="{{ url('football/images/image10.jpg') }}" class="img-responsive">
      		</div>
      		<div class="col-md-4">
      			<img src="{{ url('football/images/image11.jpg') }}" class="img-responsive">
      		</div>
      		<div class="col-md-4">
      			<img src="{{ url('football/images/image7.jpg') }}" class="img-responsive">
      		</div>
      		<div class="col-md-4">
      			<img src="{{ url('football/images/image8.jpg') }}" class="img-responsive">
      		</div>
      		<div class="col-md-4">
      			<img src="{{ url('football/images/image5.jpg') }}" class="img-responsive">
      		</div>
      		<div class="col-md-4">
      			<img src="{{ url('football/images/image6.jpg') }}" class="img-responsive">
      		</div>
      	</div>
      </div>
    </section>
    <section id="social">
      <div class="container">
        <h2 class="text-center">Follow us on our social pages!</h2>
        <div class="row">
          <div class="col-md-4">
            <a class="twitter-timeline" data-width="425" data-height="800" href="https://twitter.com/1stTouchdown">Tweets by 1stTouchdownFBCamps</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
          <div class="col-md-4"></div>
          <div class="col-md-4"></div>
        </div>
      </div>
    </section>

    <section id="contact">
      <div class="container text-center">
        <div class="row">
          <div class="col-md-6 col-sm-6">
            <p>Location:</p>
            <address>
              <strong>Joe T. Robinson High School</strong><br>
              21501 Highway 10<br>
              Little Rock, AR 72223<br>
              <!-- <abbr title="Phone">P:</abbr> (501) 000-0000<br>
              <a href="mailto:#">inquiry@example.com</a> -->
            </address>
          </div>
          <div class="col-md-6 col-sm-6">
            <p>Camp Dates:</p>
            <p>July 10-14, 2017</p>
            <p>8:00 AM - 12:00 PM</p>
          </div>
        </div>
      </div>
    </section>


@stop
