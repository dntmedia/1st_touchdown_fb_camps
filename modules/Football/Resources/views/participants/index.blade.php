@extends('backend.layouts.app')

@section('page-header')
    <h1>
        <small>Participants List</small>
    </h1>
@endsection

@section('content')
<div class="row">
<div class="col-sm-12">

<table id="table" class="table table-striped table-hover">
	<thead>
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Primary Position</th>
			<th>Secondary Position</th>
			<th>Parent Name</th>
			<th>Parent Phone</th>
			<th>Emergency Contact Name</th>
			<th>Emergency Contact Phone</th>

			<th>Actions</th>
		</tr>
	</thead>
	<tbody>
	@foreach($participants as $participant)
		<tr>
			<td>
				{{ $participant->first_name }} {{ $participant->last_name }}
			</td>
			<td>
				{{ $participant->email }}
			</td>
			<td>
				{{ $participant->primary_position }}
			</td>
			<td>
				{{ $participant->secondary_position }}
			</td>
			<td>
				{{ $participant->parent_first_name }} {{ $participant->parent_last_name }}
			</td>
			<td>
				{{ $participant->parent_phone }}
			</td>
			<td>
				{{ $participant->emergency_first_name }} {{ $participant->emergency_last_name }}
			</td>
			<td>
				{{ $participant->emergency_phone }}
			</td>			<td>
				<a href="/admin/football/participants/{{ $participant->id }}" class="btn btn-info">View More</a>
			</td>
		</tr>
	@endforeach
	</tbody>
</table>


</div>
</div>
@stop
