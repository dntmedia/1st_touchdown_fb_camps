@extends('frontend.layouts.football')

@section('content')
<section id="section-signup">
<div class="col-sm-10 col-sm-offset-1">


@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif

{!! Form::open([
    'route' => 'participants.signup'
]) !!}

<h2>Registration</h2>
<h5 class="bg-primary">Participant Information</h5>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="first_name">First Name</label>
      <input type="text" id="first_name" name="first_name" class="form-control" placeholder="First Name" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="last_name">Last Name</label>
      <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Last Name" >
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="address">Address</label>
      <input type="text" id="address" name="address" class="form-control" placeholder="Address" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="city">City</label>
      <input type="text" id="city" name="city" class="form-control" placeholder="City" >
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="state">State</label>
      <input type="text" id="state" name="state" class="form-control" placeholder="State" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="zipcode">Zip Code</label>
      <input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Zip Code" >
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="home_phone">Home Phone</label>
      <input type="text" id="home_phone" name="home_phone" class="form-control" placeholder="(000) 000-0000" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="cell_phone">Cell Phone</label>
      <input type="text" id="cell_phone" name="cell_phone" class="form-control" placeholder="(000) 000-0000" >
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="email">Email</label>
      <input type="email" id="email" name="email" class="form-control" placeholder="name@example.com" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="guardian_email">Participant / Guardian Email</label>
      <input type="email" id="guardian_email" name="guardian_email" class="form-control" placeholder="name@example.com" >
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="dob">Date of Birth</label>
      <input type="text" id="dob" name="dob" class="form-control" placeholder="01/01/1990" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="school">School</label>
      <input type="text" id="school" name="school" class="form-control" placeholder="Joe T. Robinson" >
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="primary_position">Primary Position</label>
      <select name="primary_position" id="primary_position" class="form-control">
        <option value="qb">QB</option>
        <option value="rb">RB</option>
        <option value="wr">WR</option>
        <option value="te">TE</option>
        <option value="ol">OL</option>
        <option value="dl">DL</option>
        <option value="lb">LB</option>
        <option value="s">S</option>
        <option value="cb">CB</option>
        <option value="ath">ATH</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="secondary_position">Secondary Position</label>
      <select name="secondary_position" id="secondary_position" class="form-control">
        <option value="qb">QB</option>
        <option value="rb">RB</option>
        <option value="wr">WR</option>
        <option value="te">TE</option>
        <option value="ol">OL</option>
        <option value="dl">DL</option>
        <option value="lb">LB</option>
        <option value="s">S</option>
        <option value="cb">CB</option>
        <option value="ath">ATH</option>
      </select>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="tshirt_size">T-Shirt Size</label>
      <select name="tshirt_size" id="tshirt_size" class="form-control">
        <option value="ys">YS</option>
        <option value="ym">YM</option>
        <option value="yl">YL</option>
        <option value="yxl">YXL</option>
        <option value="sm">S</option>
        <option value="md">M</option>
        <option value="lg">L</option>
        <option value="xl">XL</option>
      </select>
    </div>
  </div>
</div>

<hr>

<h5>Any pre-exisitng conditions that needs to be aware of?</h5>

<div class="form-group">
  <textarea class="form-control" rows="3" name="med_conditions" placeholder="Please provide details"></textarea>
</div>

<hr>

<h5 class="bg-primary">Parent / Guardian Information</h5>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="parent_first_name">First Name</label>
      <input type="text" id="parent_first_name" name="parent_first_name" class="form-control" placeholder="First Name" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="parent_last_name">Last Name</label>
      <input type="text" id="parent_last_name" name="parent_last_name" class="form-control" placeholder="Last Name" >
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="form-group">
      <label for="parent_phone">Phone</label>
      <input type="text" id="parent_phone" name="parent_phone" class="form-control" placeholder="(000) 000-0000" >
    </div>
  </div>
</div>

<hr>

<h5 class="bg-primary">Waiver Information</h5>

<div class="pre-scrollable">
  <p>Witness by my signature below that I have read and understood this waiver of liability and without duress, I freely agree to the following terms and conditions herein.</p>

  <p>INFORMED CONSENT/ASSUMPTION OF RISK</p>
  <p>I hereby agree to participate in one or more football camps conducted by First Touchdown FB Camps (hereinafter FTCAMPS).  FTCAMPS has made me fully aware that the activities that it offers and in which I desire to participate are of a nature and kind that are extremely strenuous and can/may push me to the limits of my physical abilities.  I recognize and understand that the camps are not without varying degrees of risk that may include but are not limited to the following:</p>

  <p>Injury to the musculoskeletal and/or cardio respiratory systems which can result in serious injury or death, injury or death due to negligence on the part of myself, my training partner, or other people around me, injury or death due to improper use or failure of equipment, or injury or death due to a medical condition, whether known or unknown by me.  I am aware that any of these above mentioned risks might result in serious injury or death to myself and or my partner(s).</p>

  <p>I willingly assume full responsibility for any and all risks that I am exposing myself to as a result of my participation in FTCAMPS clinics and/or activities and accept full responsibility for any injury or death that may result from participation in any activity, class or physical fitness program.  I herby certify that I know of no medical problems that would increase my risk of illness and injury as a result of participation in a program designed by FTCAMPS. I have been informed that there exists the possibility of adverse physical changes during a program including but not limited abnormal blood pressure, fainting, disorder of heart rhythm, stroke, and in very rare instances, heart attack or even death, and I fully understand the same.  With my full understanding of the above information, I agree to assume any and all risk associated with my participation in this FTCAMPS program.</p>

  <p>RELEASE OF LIABILITY</p>
  <p>In full consideration of the above mentioned risks and hazards and in full consideration of the fact that I am willingly and voluntarily participating in the activities made available by FTCAMPS ,and with my full understanding of all of the above, I hereby waive, release, discharge, and hold harmless FTCAMPS and its agents, officers, principals, employees, assigns and volunteers, of any and all liability, claims, demands, actions or rights of action, or damages of any kind related to, arising from, or in any way connected with, my participation in FTCAMPS programs/classes, including those allegedly attributed to the negligent  acts or omissions of the above mentioned parties. I acknowledge and affirm that I am of legal age and I have the legal authority to proceed with the above mentioned actions without having to gain permission or compensate a third party for the rights I am waiving in this section or I am a parent or guardian of a minor child and I have the legal authority to proceed with the above mentioned actions without having to gain permission or compensate a third party for the rights I am waiving in this section.</p>

  <p>This agreement shall be binding upon me, my successors, representatives, heirs, executors, assigns, or transferees. I also give permission for any certified and/or qualified person connected with FTCAMPS (either staff or a fellow patron) to administer first aid deemed necessary, and in case of serious illness or injury, I give permission to call for medical assistance and to transport me to a medical facility if deemed necessary. Furthermore I waive any and all liability, claims, demands, actions or rights of action, or damages of any kind related to, arising from, or in any way connected with medical care provided by a certified and/or qualified person(s) and hereby release said person(s) from any and all liability for their actions.</p>

  <p>INDEMNIFICATION</p>
  <p>I recognize that there is risk involved in the types of activities offered by FTCAMPS. Therefore, I accept financial responsibility for any injury that I may cause either to myself or to any other participant due to my negligence. Should FTCAMPS or its agents, officers, principals, employees, assigns and volunteers or anyone acting on its behalf, incur attorney’s fees and costs to enforce this agreement, I agree to reimburse them for such fees and costs. I further agree to indemnify and hold harmless FTCAMPS and its agents, officers, principals, employees, assigns and volunteers from liability for the injury or death of any person(s) and damage to property that may result from my negligent or intentional act or omission while participating in activities offered by FTCAMPS.

  <p>USE OF PICTURE(S)/FILM/LIKENESS</p>
  <p>I agree to allow FTCAMPS and its agents, officers, principals, employees, assigns and volunteers to use a picture(s), film and/or likeness of me for advertising purposes in perpetuity without compensation in any form of advertising currently existing or created in the future.  In the event I choose not to allow the use of the same for said purpose, I agree that I must inform FTCAMPS of this in writing. I acknowledge and affirm that I am of legal age and I have the legal authority to proceed with the above mentioned actions without having to gain permission or compensate a third party for the rights I am waiving in this section or I am a parent or guardian of a minor child and I have the legal authority to proceed with the above mentioned actions without having to gain permission or compensate a third party for the rights I am waiving in this section.

  <p>MERGER, FORUM, AND SEVERANCE</p>
  <p>This document is a complete integration of the agreement between the parties and supersedes any and all prior written or oral conversations pertaining to this matter.  This Agreement may be modified in writing only upon the consent of both parties.  Any conflicts arising as a result of this Agreement shall be resolved in a court of Arkansas jurisdiction and it shall be construed in accordance with the laws of the State of Arkansas barring all others.  If any portion of this Agreement is found to be in conflict with any federal, state, or municipal law or ordinance, the conflicting portion shall be stricken from the document and the remainder shall be enforced to the fullest extent possible.
  </p>
</div>

<div class="checkbox">
  <label>
    <input type="checkbox"> I understand and accept all of the separate event refund policies and/or waiver agreements above.
  </label>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="waiver_signature">Name (or Name of Parent/Guardian if Participant is a minor)</label>
      <input type="text" id="waiver_signature" name="waiver_signature" class="form-control" placeholder="First and Last Name" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="waiver_date">Date</label>
      <input type="date" id="waiver_date" name="waiver_date" class="form-control" placeholder="" >
    </div>
  </div>
</div>

<hr>

<h5 class="bg-primary">Emergency Contact</h5>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="emergency_first_name">First Name</label>
      <input type="text" id="emergency_first_name" name="emergency_first_name" class="form-control" placeholder="First Name" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="emergency_last_name">Last Name</label>
      <input type="text" id="emergency_last_name" name="emergency_last_name" class="form-control" placeholder="Last Name" >
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label for="emergency_phone">Phone</label>
      <input type="text" id="emergency_phone" name="emergency_phone" class="form-control" placeholder="(000) 000-0000" >
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label for="emergency_alt_phone">Alt Phone</label>
      <input type="text" id="emergency_alt_phone" name="emergency_alt_phone" class="form-control" placeholder="(000) 000-0000" >
    </div>
  </div>
</div>

        <hr>

<div class="text-center">
  <button type="submit" class="btn btn-default">Submit</button>
</div>



{!! Form::close() !!}

</div>
</section>
@stop
