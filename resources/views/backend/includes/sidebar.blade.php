<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->

{{--
        <!-- search form (Optional) -->
        {{ Form::open(['route' => 'admin.search.index', 'method' => 'get', 'class' => 'sidebar-form']) }}
        <div class="input-group">
            {{ Form::text('q', Request::get('q'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('strings.backend.general.search_placeholder')]) }}

            <span class="input-group-btn">
                    <button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span><!--input-group-btn-->
        </div><!--input-group-->
    {{ Form::close() }}
    <!-- /.search form -->
 --}}


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">

            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>
            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>


            @if ( !empty(Config::get('football.admin_module')) )
            <li class="header">{{ trans('menus.backend.football.quarter_back') }}</li>
            @role(1)
            <li class="{{ active_class(Active::checkUriPattern('admin/football/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-dollar"></i>
                    <span>{{ trans_choice('menus.backend.football.order',2) }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/football/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/football/*'), 'display: block;') }}">

                    <li class="{{ active_class(Active::checkUriPattern('admin/football/*')) }}">
                        <a href="{{ route('admin.football.orders') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans_choice('menus.backend.football.order',2) }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/football/*')) }}">
                        <a href="{{ route('admin.football.participants') }}">
                            <i class="fa fa-group"></i>
                            <span>{{ trans_choice('menus.backend.football.participant',2) }}</span>
                        </a>
                    </li>

                </ul>

            </li>
            @endauth
            @endif

            @if ( !empty(Config::get('zaiko.admin_module')) )
            <li class="header">{{ trans('menus.backend.zaiko.asset_management') }}</li>
            @role(1)
            <li class="{{ active_class(Active::checkUriPattern('hardware/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-barcode"></i>
                    <span>{{ trans_choice('menus.backend.zaiko.asset', 2) }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('hardware/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('hardware/*'), 'display: block;') }}">

                    <li class="{{ active_class(Active::checkUriPattern('hardware/user*')) }}">
                        <a href="{{ route('hardware') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.general.all') }}</span>
                        </a>
                    </li>

                    <?php $status_navs = \Modules\Zaiko\Models\Statuslabel::where('show_in_nav', '=', 1)->get();?>
                    @if (count($status_navs) > 0)
                        <li class="divider">&nbsp;</li>
                        @foreach ($status_navs as $status_nav)
                            <li class="{{ active_class(Active::checkUriPattern('hardware/?status_id=*')) }}">
                                <a href="{{ URL::to('hardware?status_id='.$status_nav->id) }}"}> {{ $status_nav->name }}</a>
                            </li>
                        @endforeach
                    @endif

                    <li class="{{ active_class(Active::checkUriPattern('hardware?status=Deployed')) }}">
                        <a href="{{ URL::to('hardware?status=Deployed') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.zaiko.deployed') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('hardware?status=RTD')) }}">
                        <a href="{{ URL::to('hardware?status=RTD') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.zaiko.ready_to_deploy') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('hardware?status=Pending')) }}">
                        <a href="{{ URL::to('hardware?status=Pending') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.zaiko.pending') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('hardware?status=Undeployable')) }}">
                        <a href="{{ URL::to('hardware?status=Undeployable') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.zaiko.undeployable') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('hardware?status=Archived')) }}">
                        <a href="{{ URL::to('hardware?status=Archived') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.zaiko.archived') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('hardware?status=Requestable')) }}">
                        <a href="{{ URL::to('hardware?status=Requestable') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.zaiko.requestable') }}</span>
                        </a>
                    </li>


                </ul>
            </li>
            @endauth
            @endif


            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>
            @role(1)
            <li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.access.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/access/role*')) }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li>
                </ul>
            </li>
            @endauth

            <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer*')) }} treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'display: block;') }}">
                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer')) }}">
                        <a href="{{ route('log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>

                    <li class="{{ active_class(Active::checkUriPattern('admin/log-viewer/logs')) }}">
                        <a href="{{ route('log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>
                </ul>
            </li>

        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>
