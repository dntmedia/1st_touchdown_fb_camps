<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', app_name())</title>

    <!-- Meta -->
    <meta name="description" content="@yield('meta_description', 'Laravel 5 Boilerplate')">
    <meta name="author" content="@yield('meta_author', 'Anthony Rappa')">
    @yield('meta')

    <!-- Styles -->
    @yield('before-styles')

    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
    @langRTL
        {{ Html::style(getRtlCss(mix('css/frontend.css'))) }}
    @else
        {{ Html::style(mix('css/frontend.css')) }}
    @endif

    @yield('after-styles')

    <link rel="stylesheet" href="{{ asset('football/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/illuminate3/css/standard.css') }}">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
    'csrfToken' => csrf_token(),
]); ?>
    </script>
</head>

<body id="app-layout">

<div>

    @include('includes.partials.logged-in-as')

    @include('frontend.includes.football_nav')

    <div class="container-fluid">

        @include('includes.partials.messages')

        @yield('content')

        <footer>
          <p class="text-center">1st Touchdown FB Camp Copyright &copy; 2017</p>
        </footer>

    </div><!-- container -->

</div><!--#app-->


<!-- Scripts -->
@yield('before-scripts')
{!! Html::script(mix('js/frontend.js')) !!}
@yield('after-scripts')

@include('includes.partials.ga')

</body>
</html>