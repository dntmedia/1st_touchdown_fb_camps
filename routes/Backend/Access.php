<?php

/**
 * All route names are prefixed with 'admin.access'.
 */
Route::group([
    'prefix' => 'access',
    'as' => 'access.',
    'namespace' => 'Access',
], function () {

    /*
     * User Management
     */
    Route::group([
        'middleware' => 'access.routeNeedsRole:1',
    ], function () {
        Route::group(['namespace' => 'User'], function () {
            /*
             * For DataTables
             */
            Route::post('user/get', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserTableController')->name('user.get');

            /*
             * User Status'
             */
            Route::get('user/deactivated', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserStatusController@getDeactivated')->name('user.deactivated');
            Route::get('user/deleted', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserStatusController@getDeleted')->name('user.deleted');

            /*
             * User CRUD
             */
            Route::resource('user', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserController');

            /*
             * Specific User
             */
            Route::group(['prefix' => 'user/{user}'], function () {
                // Account
                Route::get('account/confirm/resend', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserConfirmationController@sendConfirmationEmail')->name('user.account.confirm.resend');

                // Status
                Route::get('mark/{status}', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserStatusController@mark')->name('user.mark')->where(['status' => '[0,1]']);

                // Password
                Route::get('password/change', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserPasswordController@edit')->name('user.change-password');
                Route::patch('password/change', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserPasswordController@update')->name('user.change-password');

                // Access
                Route::get('login-as', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserAccessController@loginAs')->name('user.login-as');

                // Session
                Route::get('clear-session', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserSessionController@clearSession')->name('user.clear-session');
            });

            /*
             * Deleted User
             */
            Route::group(['prefix' => 'user/{deletedUser}'], function () {
                Route::get('delete', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserStatusController@delete')->name('user.delete-permanently');
                Route::get('restore', '\Modules\Kagi\Http\Controllers\Backend\Access\User\UserStatusController@restore')->name('user.restore');
            });
        });

        /*
         * Role Management
         */
        Route::group(['namespace' => 'Role'], function () {
            Route::resource('role', '\Modules\Kagi\Http\Controllers\Backend\Access\Role\RoleController', ['except' => ['show']]);

            //For DataTables
            Route::post('role/get', '\Modules\Kagi\Http\Controllers\Backend\Access\Role\RoleTableController')->name('role.get');
        });
    });
});
