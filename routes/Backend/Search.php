<?php

Route::group([
    'prefix' => 'search',
    'as' => 'search.',
    'namespace' => 'Search',
], function () {

    /*
     * Search Specific Functionality
     */
    Route::get('/', '\Modules\Kensaku\Http\Controllers\SearchController@index')->name('index');
});
