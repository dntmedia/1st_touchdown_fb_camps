<?php

// dd(Config::get('basic.over_ride'));

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'.
 */
// if (Config::get('basic.over_ride') === 0) {

//     Route::get('/', '\Modules\Basic\Http\Controllers\FrontendController@index')->name('index');
//     Route::get('macros', '\Modules\Basic\Http\Controllers\FrontendController@macros')->name('macros');

// }

/*
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
        /*
         * User Dashboard Specific
         */
        Route::get('dashboard', '\Modules\Basic\Http\Controllers\DashboardController@index')->name('dashboard');

        /*
         * User Account Specific
         */
        Route::get('account', '\Modules\Kagi\Http\Controllers\Frontend\User\AccountController@index')->name('account');

        /*
         * User Profile Specific
         */
        Route::patch('profile/update', '\Modules\Kagi\Http\Controllers\Frontend\User\ProfileController@update')->name('profile.update');
    });
});
