<?php

/**
 * Frontend Access Controllers
 * All route names are prefixed with 'frontend.auth'.
 */
Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {

    /*
     * These routes require the user to be logged in
     */
    Route::group(['middleware' => 'auth'], function () {
        Route::get('logout', '\Modules\Kagi\Http\Controllers\Frontend\Auth\LoginController@logout')->name('logout');

        //For when admin is logged in as user from backend
        Route::get('logout-as', '\Modules\Kagi\Http\Controllers\Frontend\Auth\LoginController@logoutAs')->name('logout-as');

        // Change Password Routes
        Route::patch('password/change', '\Modules\Kagi\Http\Controllers\Frontend\Auth\ChangePasswordController@changePassword')->name('password.change');
    });

    /*
     * These routes require no user to be logged in
     */
    Route::group(['middleware' => 'guest'], function () {
        // Authentication Routes
        Route::get('login', '\Modules\Kagi\Http\Controllers\Frontend\Auth\LoginController@showLoginForm')->name('login');
        Route::post('login', '\Modules\Kagi\Http\Controllers\Frontend\Auth\LoginController@login')->name('login');

        // Socialite Routes
        Route::get('login/{provider}', '\Modules\Kagi\Http\Controllers\Frontend\Auth\SocialLoginController@login')->name('social.login');

        // Registration Routes
        if (config('access.users.registration')) {
            Route::get('register', '\Modules\Kagi\Http\Controllers\Frontend\Auth\RegisterController@showRegistrationForm')->name('register');
            Route::post('register', '\Modules\Kagi\Http\Controllers\Frontend\Auth\RegisterController@register')->name('register');
        }

        // Confirm Account Routes
        Route::get('account/confirm/{token}', '\Modules\Kagi\Http\Controllers\Frontend\Auth\ConfirmAccountController@confirm')->name('account.confirm');
        Route::get('account/confirm/resend/{user}', '\Modules\Kagi\Http\Controllers\Frontend\Auth\ConfirmAccountController@sendConfirmationEmail')->name('account.confirm.resend');

        // Password Reset Routes
        Route::get('password/reset', '\Modules\Kagi\Http\Controllers\Frontend\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');
        Route::post('password/email', '\Modules\Kagi\Http\Controllers\Frontend\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

        Route::get('password/reset/{token}', '\Modules\Kagi\Http\Controllers\Frontend\Auth\ResetPasswordController@showResetForm')->name('password.reset.form');
        Route::post('password/reset', '\Modules\Kagi\Http\Controllers\Frontend\Auth\ResetPasswordController@reset')->name('password.reset');
    });
});
